# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the ksystemstats package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ksystemstats\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-24 09:33+0000\n"
"PO-Revision-Date: 2022-11-27 11:31+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2\n"

#: cpu/cpu.cpp:28 disks/disks.cpp:67 disks/disks.cpp:68 gpu/GpuDevice.cpp:20
#: power/power.cpp:39
#, kde-format
msgctxt "@title"
msgid "Name"
msgstr "სახელი"

#: cpu/cpu.cpp:38
#, kde-format
msgctxt "@title"
msgid "Total Usage"
msgstr "სულ გამოყენება"

#: cpu/cpu.cpp:39
#, kde-format
msgctxt "@title, Short for 'Total Usage'"
msgid "Usage"
msgstr "გამოყენება"

#: cpu/cpu.cpp:45
#, kde-format
msgctxt "@title"
msgid "System Usage"
msgstr "სისტემური გამოყენება"

#: cpu/cpu.cpp:46
#, kde-format
msgctxt "@title, Short for 'System Usage'"
msgid "System"
msgstr "სისტემა"

#: cpu/cpu.cpp:52
#, kde-format
msgctxt "@title"
msgid "User Usage"
msgstr "მომხმარებლის გამოყენება"

#: cpu/cpu.cpp:53
#, kde-format
msgctxt "@title, Short for 'User Usage'"
msgid "User"
msgstr "User"

#: cpu/cpu.cpp:59
#, kde-format
msgctxt "@title"
msgid "Wait Usage"
msgstr "ლოდინის გამოყენება"

#: cpu/cpu.cpp:60
#, kde-format
msgctxt "@title, Short for 'Wait Load'"
msgid "Wait"
msgstr "მოცდა"

#: cpu/cpu.cpp:85
#, kde-format
msgctxt "@title"
msgid "Current Frequency"
msgstr "მიმდინარე სიხშირე"

#: cpu/cpu.cpp:86
#, kde-format
msgctxt "@title, Short for 'Current Frequency'"
msgid "Frequency"
msgstr "სიხშირე"

#: cpu/cpu.cpp:87
#, kde-format
msgctxt "@info"
msgid "Current frequency of the CPU"
msgstr "CPU- ს მიმდინარე სიხშირე"

#: cpu/cpu.cpp:92
#, kde-format
msgctxt "@title"
msgid "Current Temperature"
msgstr "მიმდინარე ტემპერატურა"

#: cpu/cpu.cpp:93
#, kde-format
msgctxt "@title, Short for Current Temperatur"
msgid "Temperature"
msgstr "ტემპერატურა"

#: cpu/cpu.cpp:100
#, kde-format
msgctxt "@title"
msgid "All"
msgstr "&ყველა"

#: cpu/cpu.cpp:119
#, kde-format
msgctxt "@title"
msgid "Maximum CPU Frequency"
msgstr "CPU -ის მაქსიმალური სიხშირე"

#: cpu/cpu.cpp:120
#, kde-format
msgctxt "@title, Short for 'Maximum CPU Frequency'"
msgid "Max Frequency"
msgstr "მაქსიმალური სიხშირე"

#: cpu/cpu.cpp:121
#, kde-format
msgctxt "@info"
msgid "Current maximum frequency between all CPUs"
msgstr "მიმდინარე მაქსიმალური სიხშირე ყველა CPO- ს შორის"

#: cpu/cpu.cpp:126
#, kde-format
msgctxt "@title"
msgid "Minimum CPU Frequency"
msgstr "CPU-ის მინიმალური სიხშირე"

#: cpu/cpu.cpp:127
#, kde-format
msgctxt "@title, Short for 'Minimum CPU Frequency'"
msgid "Min Frequency"
msgstr "მინიმალური სიხშირე"

#: cpu/cpu.cpp:128
#, kde-format
msgctxt "@info"
msgid "Current minimum frequency between all CPUs"
msgstr "მიმდინარე მინიმალური სიხშირე ყველა CPU- ს შორის"

#: cpu/cpu.cpp:133
#, kde-format
msgctxt "@title"
msgid "Average CPU Frequency"
msgstr "CPU-ის საშუალო სიხშირე"

#: cpu/cpu.cpp:134
#, kde-format
msgctxt "@title, Short for 'Average CPU Frequency'"
msgid "Average Frequency"
msgstr "საშუალო სიხშირე"

#: cpu/cpu.cpp:135
#, kde-format
msgctxt "@info"
msgid "Current average frequency between all CPUs"
msgstr "მიმდინარე საშუალო სიხშირე ყველა CPU-ს ს შორის"

#: cpu/cpu.cpp:140
#, kde-format
msgctxt "@title"
msgid "Maximum CPU Temperature"
msgstr "CPU-ის მაქსიმალური ტემპერატურა"

#: cpu/cpu.cpp:141
#, kde-format
msgctxt "@title, Short for 'Maximum CPU Temperature'"
msgid "Max Temperature"
msgstr "მაქსიმალური ტემპერატურა"

#: cpu/cpu.cpp:147
#, kde-format
msgctxt "@title"
msgid "Minimum CPU Temperature"
msgstr "CPU-ის მინიმალური ტემპერატურა"

#: cpu/cpu.cpp:148
#, kde-format
msgctxt "@title, Short for 'Minimum CPU Temperature'"
msgid "Min Temperature"
msgstr "მინიმალური ტემპერატურა"

#: cpu/cpu.cpp:154
#, kde-format
msgctxt "@title"
msgid "Average CPU Temperature"
msgstr "CPU-ის საშუალო ტემპერატურა"

#: cpu/cpu.cpp:155
#, kde-format
msgctxt "@title, Short for 'Average CPU Temperature'"
msgid "Average Temperature"
msgstr "საშუალო ტემპერატურა"

#: cpu/cpu.cpp:171
#, kde-format
msgctxt "@title"
msgid "Number of CPUs"
msgstr "CPU-ების რიცხვი"

#: cpu/cpu.cpp:172
#, kde-format
msgctxt "@title, Short fort 'Number of CPUs'"
msgid "CPUs"
msgstr "CPU"

#: cpu/cpu.cpp:173
#, kde-format
msgctxt "@info"
msgid "Number of physical CPUs installed in the system"
msgstr "სისტემაში დაყენებული ფიზიკური CPU-ების რაოდენობა"

#: cpu/cpu.cpp:175
#, kde-format
msgctxt "@title"
msgid "Number of Cores"
msgstr "ბირთვების რაოდენობა"

#: cpu/cpu.cpp:176
#, kde-format
msgctxt "@title, Short fort 'Number of Cores'"
msgid "Cores"
msgstr "ბირთვები"

#: cpu/cpu.cpp:177
#, kde-format
msgctxt "@info"
msgid "Number of CPU cores across all physical CPUS"
msgstr "CPU ბირთვების რაოდენობა ყველა ფიზიკურ CPU-ში"

#: cpu/cpuplugin.cpp:19
#, kde-format
msgid "CPUs"
msgstr "CPU"

#: cpu/freebsdcpuplugin.cpp:151
#, kde-format
msgctxt "@title"
msgid "CPU %1"
msgstr "CPU %1"

#: cpu/linuxcpuplugin.cpp:39
#, kde-format
msgctxt "@title"
msgid "Core %1"
msgstr "ბირთვი %1"

#: cpu/linuxcpuplugin.cpp:54
#, kde-format
msgctxt "@title"
msgid "CPU %1 Core %2"
msgstr "CPU %1 ბირთვი %2"

#: cpu/loadaverages.cpp:13
#, kde-format
msgctxt "@title"
msgid "Load Averages"
msgstr "საშუალო დატვირთვები"

#: cpu/loadaverages.cpp:14
#, kde-format
msgctxt "@title"
msgid "Load average (1 minute)"
msgstr "საშუალო დატვირთვა (1 წუთი)"

#: cpu/loadaverages.cpp:15
#, kde-format
msgctxt "@title"
msgid "Load average (5 minutes)"
msgstr "საშუალო დატვირთვა (5 წუთი)"

#: cpu/loadaverages.cpp:16
#, kde-format
msgctxt "@title"
msgid "Load average (15 minute)"
msgstr "საშუალო დატვირთვა (15 წუთი)"

#: cpu/loadaverages.cpp:18
#, kde-format
msgctxt "@title,  Short for 'Load average (1 minute)"
msgid "Load average (1m)"
msgstr "საშუალო დატვირთვა (1 წთ)"

#: cpu/loadaverages.cpp:19
#, kde-format
msgctxt "@title,  Short for 'Load average (5 minutes)"
msgid "Load average (5m)"
msgstr "საშუალო დატვირთვა (5 წუთი)"

#: cpu/loadaverages.cpp:20
#, kde-format
msgctxt "@title,  Short for 'Load average (15 minutes)"
msgid "Load average (15m)"
msgstr "საშუალო დატვირთვა (15 წუთი)"

#: cpu/loadaverages.cpp:22
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 1 minute"
msgstr "1 წუთის განმავლობაში რიგში გაშვებული დავალებების საშუალო რაოდენობა"

#: cpu/loadaverages.cpp:23
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 5 minutes"
msgstr "5 წუთის განმავლობაში რიგში გაშვებული დავალებების საშუალო რაოდენობა"

#: cpu/loadaverages.cpp:24
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 15 minutes"
msgstr "15 წუთის განმავლობაში რიგში გაშვებული დავალებების საშუალო რაოდენობა"

#: disks/disks.cpp:71 disks/disks.cpp:236
#, kde-format
msgctxt "@title"
msgid "Total Space"
msgstr "ჯამური სივრცე"

#: disks/disks.cpp:73 disks/disks.cpp:237
#, kde-format
msgctxt "@title Short for 'Total Space'"
msgid "Total"
msgstr "ჯამში"

#: disks/disks.cpp:77 disks/disks.cpp:249
#, kde-format
msgctxt "@title"
msgid "Used Space"
msgstr "გამოყენებული ადგილი დისკზე"

#: disks/disks.cpp:79 disks/disks.cpp:250
#, kde-format
msgctxt "@title Short for 'Used Space'"
msgid "Used"
msgstr "Გამოყენებული"

#: disks/disks.cpp:84 disks/disks.cpp:242
#, kde-format
msgctxt "@title"
msgid "Free Space"
msgstr "თავისუფალი ადგილი"

#: disks/disks.cpp:86 disks/disks.cpp:243
#, kde-format
msgctxt "@title Short for 'Free Space'"
msgid "Free"
msgstr "თავისუფალი"

#: disks/disks.cpp:91 disks/disks.cpp:256
#, kde-format
msgctxt "@title"
msgid "Read Rate"
msgstr "კითხვის სიჩქარე"

#: disks/disks.cpp:93 disks/disks.cpp:257
#, kde-format
msgctxt "@title Short for 'Read Rate'"
msgid "Read"
msgstr "წაკითხვა"

#: disks/disks.cpp:97 disks/disks.cpp:262
#, kde-format
msgctxt "@title"
msgid "Write Rate"
msgstr "ჩაწერის სიჩქარე"

#: disks/disks.cpp:99 disks/disks.cpp:263
#, kde-format
msgctxt "@title Short for 'Write Rate'"
msgid "Write"
msgstr "ჩასწორება"

#: disks/disks.cpp:103 disks/disks.cpp:272
#, kde-format
msgctxt "@title"
msgid "Percentage Used"
msgstr "გამოყენება პროცენტებში"

#: disks/disks.cpp:107 disks/disks.cpp:268
#, kde-format
msgctxt "@title"
msgid "Percentage Free"
msgstr "თავისუფალი, პროცენტებში"

#: disks/disks.cpp:142
#, kde-format
msgid "Disks"
msgstr "დისკები"

#: disks/disks.cpp:234
#, kde-format
msgctxt "@title"
msgid "All Disks"
msgstr "ყველა დისკი"

#: disks/disks.cpp:269
#, kde-format
msgctxt "@title, Short for `Percentage Free"
msgid "Free"
msgstr "თავისუფალი"

#: disks/disks.cpp:273
#, kde-format
msgctxt "@title, Short for `Percentage Used"
msgid "Used"
msgstr "Გამოყენებული"

#: gpu/AllGpus.cpp:14
#, kde-format
msgctxt "@title"
msgid "All GPUs"
msgstr "ყველა GPU"

#: gpu/AllGpus.cpp:16
#, kde-format
msgctxt "@title"
msgid "All GPUs Usage"
msgstr "ყველა GPU-ის გამოყენება"

#: gpu/AllGpus.cpp:17
#, kde-format
msgctxt "@title Short for 'All GPUs Usage'"
msgid "Usage"
msgstr "გამოყენება"

#: gpu/AllGpus.cpp:26
#, kde-format
msgctxt "@title"
msgid "All GPUs Total Memory"
msgstr "ყველა GPU-ის ჯამური მეხსიერება"

#: gpu/AllGpus.cpp:27
#, kde-format
msgctxt "@title Short for 'All GPUs Total Memory'"
msgid "Total"
msgstr "ჯამში"

#: gpu/AllGpus.cpp:31
#, kde-format
msgctxt "@title"
msgid "All GPUs Used Memory"
msgstr "ყველა GPU-ის ჯამური გამოყენებული მეხსიერება"

#: gpu/AllGpus.cpp:32
#, kde-format
msgctxt "@title Short for 'All GPUs Used Memory'"
msgid "Used"
msgstr "Გამოყენებული"

#: gpu/GpuDevice.cpp:24
#, kde-format
msgctxt "@title"
msgid "Usage"
msgstr "გამოყენება"

#: gpu/GpuDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "Total Video Memory"
msgstr "ჯამური ვიდეომეხსიერება"

#: gpu/GpuDevice.cpp:32
#, kde-format
msgctxt "@title Short for Total Video Memory"
msgid "Total"
msgstr "ჯამში"

#: gpu/GpuDevice.cpp:35
#, kde-format
msgctxt "@title"
msgid "Video Memory Used"
msgstr "გამოყენებული ვიდეომეხსიერება"

#: gpu/GpuDevice.cpp:37
#, kde-format
msgctxt "@title Short for Video Memory Used"
msgid "Used"
msgstr "Გამოყენებული"

#: gpu/GpuDevice.cpp:41
#, kde-format
msgctxt "@title"
msgid "Frequency"
msgstr "სიხშირე"

#: gpu/GpuDevice.cpp:45
#, kde-format
msgctxt "@title"
msgid "Memory Frequency"
msgstr "მეხსიერების სიხშირე"

#: gpu/GpuDevice.cpp:49
#, kde-format
msgctxt "@title"
msgid "Temperature"
msgstr "ტემპერატურა"

#: gpu/GpuDevice.cpp:53 power/power.cpp:106
#, kde-format
msgctxt "@title"
msgid "Power"
msgstr "კვება"

#: gpu/GpuPlugin.cpp:31
#, kde-format
msgctxt "@title"
msgid "GPU"
msgstr "GPU"

#: gpu/LinuxBackend.cpp:54
#, kde-format
msgctxt "@title %1 is GPU number"
msgid "GPU %1"
msgstr "GPU %1"

#: lmsensors/lmsensors.cpp:22
#, kde-format
msgid "Hardware Sensors"
msgstr "აპარატურული სენსორები"

#: memory/backend.cpp:17
#, kde-format
msgctxt "@title"
msgid "Physical Memory"
msgstr "ფიზიკური მეხსიერება"

#: memory/backend.cpp:18
#, kde-format
msgctxt "@title"
msgid "Swap Memory"
msgstr "სვოპის მეხსიერება"

#: memory/backend.cpp:39
#, kde-format
msgctxt "@title"
msgid "Total Physical Memory"
msgstr "ჯამური ფიზიკური მეხსიერება"

#: memory/backend.cpp:40
#, kde-format
msgctxt "@title, Short for 'Total Physical Memory'"
msgid "Total"
msgstr "ჯამში"

#: memory/backend.cpp:44
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory"
msgstr "გამოყენებული ფიზიკური მეხსიერება"

#: memory/backend.cpp:45
#, kde-format
msgctxt "@title, Short for 'Used Physical Memory'"
msgid "Used"
msgstr "Გამოყენებული"

#: memory/backend.cpp:49
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory Percentage"
msgstr "გამოყენებული ფიზიკური მეხსიერება, პროცენტებში"

#: memory/backend.cpp:53
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory"
msgstr "თავისუფალი ფიზიკური მეხსიერება"

#: memory/backend.cpp:54
#, kde-format
msgctxt "@title, Short for 'Free Physical Memory'"
msgid "Free"
msgstr "თავისუფალი"

#: memory/backend.cpp:58
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory Percentage"
msgstr "თავისუფალი ფიზიკური მეხსიერება, პროცენტებში"

#: memory/backend.cpp:62
#, kde-format
msgctxt "@title"
msgid "Application Memory"
msgstr "აპლიკაციის მეხსიერება"

#: memory/backend.cpp:63
#, kde-format
msgctxt "@title, Short for 'Application Memory'"
msgid "Application"
msgstr "აპლიკაცია"

#: memory/backend.cpp:67
#, kde-format
msgctxt "@title"
msgid "Application Memory Percentage"
msgstr "აპლიკაციის მეხსიერება, პროცენტებში"

#: memory/backend.cpp:71
#, kde-format
msgctxt "@title"
msgid "Cache Memory"
msgstr "ქეშის მეხსიერება"

#: memory/backend.cpp:72
#, kde-format
msgctxt "@title, Short for 'Cache Memory'"
msgid "Cache"
msgstr "კეში"

#: memory/backend.cpp:76
#, kde-format
msgctxt "@title"
msgid "Cache Memory Percentage"
msgstr "ქეშის მეხსიერება, პროცენტებში"

#: memory/backend.cpp:80
#, kde-format
msgctxt "@title"
msgid "Buffer Memory"
msgstr "ბაფერის მეხსიერება"

#: memory/backend.cpp:81
#, kde-format
msgctxt "@title, Short for 'Buffer Memory'"
msgid "Buffer"
msgstr "ბaფერი"

#: memory/backend.cpp:82
#, kde-format
msgid "Amount of memory used for caching disk blocks"
msgstr "დისკის ბლოკების ქეშისთვის გამოყენებული მეხსიერების რაოდენობა"

#: memory/backend.cpp:86
#, kde-format
msgctxt "@title"
msgid "Buffer Memory Percentage"
msgstr "ბაფერის მეხსიერება, პროცენტებში"

#: memory/backend.cpp:90
#, kde-format
msgctxt "@title"
msgid "Total Swap Memory"
msgstr "სვოპის ჯამური მეხსიერება"

#: memory/backend.cpp:91
#, kde-format
msgctxt "@title, Short for 'Total Swap Memory'"
msgid "Total"
msgstr "ჯამში"

#: memory/backend.cpp:95
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory"
msgstr "გამოყენებული სვოპის მეხსიერება"

#: memory/backend.cpp:96
#, kde-format
msgctxt "@title, Short for 'Used Swap Memory'"
msgid "Used"
msgstr "Გამოყენებული"

#: memory/backend.cpp:100
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory Percentage"
msgstr "გამოყენებული სვოპის მეხსიერება, პროცენტებში"

#: memory/backend.cpp:104
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory"
msgstr "თავისუფალი სვოპის მეხსიერება"

#: memory/backend.cpp:105
#, kde-format
msgctxt "@title, Short for 'Free Swap Memory'"
msgid "Free"
msgstr "თავისუფალი"

#: memory/backend.cpp:109
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory Percentage"
msgstr "თავისუფალი სვოპის მეხსიერება, პროცენტებში"

#: memory/memory.cpp:24
#, kde-format
msgctxt "@title"
msgid "Memory"
msgstr "მეხსიერება"

#: network/AllDevicesObject.cpp:16
#, kde-format
msgctxt "@title"
msgid "All Network Devices"
msgstr "ყველა ქსელური მოწყობილობა"

#: network/AllDevicesObject.cpp:18 network/AllDevicesObject.cpp:28
#: network/NetworkDevice.cpp:66 network/NetworkDevice.cpp:76
#, kde-format
msgctxt "@title"
msgid "Download Rate"
msgstr "გადმოწერის სიჩქარე"

#: network/AllDevicesObject.cpp:19 network/AllDevicesObject.cpp:29
#: network/NetworkDevice.cpp:67 network/NetworkDevice.cpp:77
#, kde-format
msgctxt "@title Short for Download Rate"
msgid "Download"
msgstr "ჩამოტვირთვა"

#: network/AllDevicesObject.cpp:23 network/AllDevicesObject.cpp:33
#: network/NetworkDevice.cpp:71 network/NetworkDevice.cpp:81
#, kde-format
msgctxt "@title"
msgid "Upload Rate"
msgstr "ატვირთვის სიჩქარე"

#: network/AllDevicesObject.cpp:24 network/AllDevicesObject.cpp:34
#: network/NetworkDevice.cpp:72 network/NetworkDevice.cpp:82
#, kde-format
msgctxt "@title Short for Upload Rate"
msgid "Upload"
msgstr "ატვირთვა"

#: network/AllDevicesObject.cpp:38 network/NetworkDevice.cpp:86
#, kde-format
msgctxt "@title"
msgid "Total Downloaded"
msgstr "სულ გადმოწერილი"

#: network/AllDevicesObject.cpp:39 network/NetworkDevice.cpp:87
#, kde-format
msgctxt "@title Short for Total Downloaded"
msgid "Downloaded"
msgstr "გადმოწერილია"

#: network/AllDevicesObject.cpp:43 network/NetworkDevice.cpp:91
#, kde-format
msgctxt "@title"
msgid "Total Uploaded"
msgstr "სულ ატვირთული"

#: network/AllDevicesObject.cpp:44 network/NetworkDevice.cpp:92
#, kde-format
msgctxt "@title Short for Total Uploaded"
msgid "Uploaded"
msgstr "ატვირთულია"

#: network/NetworkDevice.cpp:15
#, kde-format
msgctxt "@title"
msgid "Network Name"
msgstr "ქსელის დასახელება"

#: network/NetworkDevice.cpp:16
#, kde-format
msgctxt "@title Short of Network Name"
msgid "Name"
msgstr "სახელი"

#: network/NetworkDevice.cpp:19
#, kde-format
msgctxt "@title"
msgid "Signal Strength"
msgstr "სიგნალის სიძლიერე"

#: network/NetworkDevice.cpp:20
#, kde-format
msgctxt "@title Short of Signal Strength"
msgid "Signal"
msgstr "სიგნალი"

#: network/NetworkDevice.cpp:26
#, kde-format
msgctxt "@title"
msgid "IPv4 Address"
msgstr "IPv4 მისამართი"

#: network/NetworkDevice.cpp:27
#, kde-format
msgctxt "@title Short of IPv4 Address"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "IPv4 Gateway"
msgstr "IPv4 ნაგულისხმები რაუტერი"

#: network/NetworkDevice.cpp:31
#, kde-format
msgctxt "@title Short of IPv4 Gateway"
msgid "IPv4 Gateway"
msgstr "IPv4 ნაგულისხმები რაუტერი"

#: network/NetworkDevice.cpp:34
#, kde-format
msgctxt "@title"
msgid "IPv4 Subnet Mask"
msgstr "IPv4 ქვექსელის ნიღაბი"

#: network/NetworkDevice.cpp:35
#, kde-format
msgctxt "@title Short of IPv4 Subnet Mask"
msgid "IPv4 Subnet Mask"
msgstr "IPv4 ქვექსელის ნიღაბი"

#: network/NetworkDevice.cpp:38
#, kde-format
msgctxt "@title"
msgid "IPv4 with Prefix Length"
msgstr "IPv4 პრეფიქსის სიგრძით"

#: network/NetworkDevice.cpp:39
#, kde-format
msgctxt "@title Short of IPv4 Prefix Length"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:42
#, kde-format
msgctxt "@title"
msgid "IPv4 DNS"
msgstr "IPv4 DNS"

#: network/NetworkDevice.cpp:43
#, kde-format
msgctxt "@title Short of IPv4 DNS"
msgid "IPv4 DNS"
msgstr "IPv4 DNS"

#: network/NetworkDevice.cpp:46
#, kde-format
msgctxt "@title"
msgid "IPv6 Address"
msgstr "IPv6 მისამართი"

#: network/NetworkDevice.cpp:47
#, kde-format
msgctxt "@title Short of IPv6 Address"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:50
#, kde-format
msgctxt "@title"
msgid "IPv6 Gateway"
msgstr "IPv6 ნაგულისხმები რაუტერი"

#: network/NetworkDevice.cpp:51
#, kde-format
msgctxt "@title Short of IPv6 Gateway"
msgid "IPv6 Gateway"
msgstr "IPv6 ნაგულისხმები რაუტერი"

#: network/NetworkDevice.cpp:54
#, kde-format
msgctxt "@title"
msgid "IPv6 Subnet Mask"
msgstr "IPv6 ქვექსელის ნიღაბი"

#: network/NetworkDevice.cpp:55
#, kde-format
msgctxt "@title Short of IPv6 Subnet Mask"
msgid "IPv6 Subnet Mask"
msgstr "IPv6 ქვექსელის ნიღაბი"

#: network/NetworkDevice.cpp:58
#, kde-format
msgctxt "@title"
msgid "IPv6 with Prefix Length"
msgstr "IPv6 პრეფიქსის სიგრძით"

#: network/NetworkDevice.cpp:59
#, kde-format
msgctxt "@title Short of IPv6 Prefix Length"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:62
#, kde-format
msgctxt "@title"
msgid "IPv6 DNS"
msgstr "IPv6 DNS"

#: network/NetworkDevice.cpp:63
#, kde-format
msgctxt "@title Short of IPv6 DNS"
msgid "IPv6 DNS"
msgstr "IPv6 DNS"

#: network/NetworkPlugin.cpp:43
#, kde-format
msgctxt "@title"
msgid "Network Devices"
msgstr "ქსელის მოწყობილობები"

#: osinfo/osinfo.cpp:106
#, kde-format
msgctxt "@title"
msgid "Operating System"
msgstr "ოპერაციული სისტემა"

#: osinfo/osinfo.cpp:108
#, kde-format
msgctxt "@title"
msgid "Kernel"
msgstr "ბირთვი"

#: osinfo/osinfo.cpp:109
#, kde-format
msgctxt "@title"
msgid "Kernel Name"
msgstr "ბირთვის სახელი"

#: osinfo/osinfo.cpp:110
#, kde-format
msgctxt "@title"
msgid "Kernel Version"
msgstr "ბირთვის ვერსია"

#: osinfo/osinfo.cpp:111
#, kde-format
msgctxt "@title"
msgid "Kernel Name and Version"
msgstr "ბირთვის სახელი და ვერსია"

#: osinfo/osinfo.cpp:112
#, kde-format
msgctxt "@title Kernel Name and Version"
msgid "Kernel"
msgstr "ბირთვი"

#: osinfo/osinfo.cpp:114
#, kde-format
msgctxt "@title"
msgid "System"
msgstr "სისტემა"

#: osinfo/osinfo.cpp:115
#, kde-format
msgctxt "@title"
msgid "Hostname"
msgstr "ჰოსტის სახელი"

#: osinfo/osinfo.cpp:116
#, kde-format
msgctxt "@title"
msgid "Operating System Name"
msgstr "ოპერაციული სისტემის სახელი"

#: osinfo/osinfo.cpp:117
#, kde-format
msgctxt "@title"
msgid "Operating System Version"
msgstr "ოპერაციული სისტემის ვერსია"

#: osinfo/osinfo.cpp:118
#, kde-format
msgctxt "@title"
msgid "Operating System Name and Version"
msgstr "ოპერაციული სისტემის სახელი და ვერსია"

#: osinfo/osinfo.cpp:119
#, kde-format
msgctxt "@title Operating System Name and Version"
msgid "OS"
msgstr "ოს-ი"

#: osinfo/osinfo.cpp:120
#, kde-format
msgctxt "@title"
msgid "Operating System Logo"
msgstr "ოპერაციული სისტემის ლოგო"

#: osinfo/osinfo.cpp:121
#, kde-format
msgctxt "@title"
msgid "Operating System URL"
msgstr "ოპერაციული სისტემის URL"

#: osinfo/osinfo.cpp:122
#, kde-format
msgctxt "@title"
msgid "Uptime"
msgstr "ჩართულია"

#: osinfo/osinfo.cpp:125
#, kde-format
msgctxt "@title"
msgid "KDE Plasma"
msgstr "KDE plasma"

#: osinfo/osinfo.cpp:126
#, kde-format
msgctxt "@title"
msgid "Qt Version"
msgstr "Qt-ის ვერსია"

#: osinfo/osinfo.cpp:127
#, kde-format
msgctxt "@title"
msgid "KDE Frameworks Version"
msgstr "KDE Frameworks-ის ვერსია"

#: osinfo/osinfo.cpp:128
#, kde-format
msgctxt "@title"
msgid "KDE Plasma Version"
msgstr "KDE Plasma-ის ვერსია"

#: osinfo/osinfo.cpp:129
#, kde-format
msgctxt "@title"
msgid "Window System"
msgstr "ფანჯრის სისტემა"

#: osinfo/osinfo.cpp:163
#, kde-format
msgctxt "@info"
msgid "Unknown"
msgstr "უცნობი"

#: power/power.cpp:44 power/power.cpp:45
#, kde-format
msgctxt "@title"
msgid "Design Capacity"
msgstr "დიზაინის მოცულობა"

#: power/power.cpp:47
#, kde-format
msgid "Amount of energy that the Battery was designed to hold"
msgstr "ენერგია, რომლის გამოსაცემადაც შეიქმნა ეს ელემენტი"

#: power/power.cpp:53 power/power.cpp:54 power/power.cpp:72
#, kde-format
msgctxt "@title"
msgid "Current Capacity"
msgstr "მიმდინარე მოცულობა"

#: power/power.cpp:56
#, kde-format
msgid "Amount of energy that the battery can currently hold"
msgstr "ენერგია, რომლის გამოცემაც ელემენტს ამჟამად შეუძლია"

#: power/power.cpp:62 power/power.cpp:63
#, kde-format
msgctxt "@title"
msgid "Health"
msgstr "ჯანმრთელობა"

#: power/power.cpp:65
#, kde-format
msgid "Percentage of the design capacity that the battery can hold"
msgstr ""
"პროცენტულობა აგებისას მითითებული მოცულობიდან, რომელიც ელემენტს შეუძლია, "
"შეინარჩუნოს"

#: power/power.cpp:71
#, kde-format
msgctxt "@title"
msgid "Charge"
msgstr "მუხტი"

#: power/power.cpp:74
#, kde-format
msgid "Amount of energy that the battery is currently holding"
msgstr "ენერგიის რაოდენობა, რომელიც ელემენტს ამჟამად გააჩნია"

#: power/power.cpp:80 power/power.cpp:81
#, kde-format
msgctxt "@title"
msgid "Charge Percentage"
msgstr "დატენვის პროცენტულობა"

#: power/power.cpp:83
#, kde-format
msgid ""
"Percentage of the current capacity that the battery is currently holding"
msgstr ""
"ელემენტის მიმდინარე მოცულობასთან შედარებით მისი მიმდინარე მუხტის მოცულობა, "
"პროცენტებში"

#: power/power.cpp:90
#, kde-format
msgctxt "@title"
msgid "Charging Rate"
msgstr "დატენვის სიჩქარე"

#: power/power.cpp:91
#, kde-format
msgctxt "@title"
msgid "Charging  Rate"
msgstr "დატენვის სიჩქარე"

#: power/power.cpp:93
#, kde-format
msgid ""
"Power that the battery is being charged with (positive) or discharged "
"(negative)"
msgstr ""
"ენერგია, რომლითაც ელემენტი იტენება (დადებითი) ან განიმუხტება (უარყოფითი)"
